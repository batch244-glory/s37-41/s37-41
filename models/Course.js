const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema(
	{
		name: {
			type: String,

			// Requires the data for this field to be included when creating a record
			// "true" - defines if the field is required or not and the second element is the message that will be printed out in our terminal when the data is not present
			required: [true, "Course name is required."]
		},
		description: {
			type: String,
			required: [true, "Description is required."]
		},
		price: {
			type: Number,
			required: [true, "Price is required."]
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			// "new Date()" instantiates a new "date" that stores the current date and time whenever a course is created in the database
			default: new Date()
		},
		// We applied the concept of referencing data to establish the relationship between our courses and users
		enrollees : [
			{
				userId: {
					type: String,
					required: [true, "UserID is required."]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
)

module.exports = mongoose.model("Course", courseSchema);